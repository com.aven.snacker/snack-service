# Snack Service
It's a microservice used in Aven-Snacker microservice architecture. This service is responsible for handling all operations concerning snacks(fast food restaurant).Those operations are used in summary to create handle or delete snacks and customers.
To performs all those operations the service provide two API (**Admin API** and Public Api).
# 1-key-features 
As an admin we can performs, throught the service, the following operations:

- [create new snack](https://gitlab.com/com.aven.snacker/snack-service/-/wikis/docs/snack-service/admin/create-snack)
- [find one or many snacks](https://gitlab.com/com.aven.snacker/snack-service/-/wikis/docs/snack-service/admin/find-snacks)
- [edit snack informations](https://gitlab.com/com.aven.snacker/snack-service/-/wikis/docs/snack-service/admin/edit-snack)
- [delete one or many snacks](https://gitlab.com/com.aven.snacker/snack-service/-/wikis/docs/snack-service/admin/delete-snacks)
- [edit snack states](https://gitlab.com/com.aven.snacker/snack-service/-/wikis/docs/snack-service/admin/edit-state-snack)
- [get one or many customers from snacks](https://gitlab.com/com.aven.snacker/snack-service/-/wikis/docs/snack-service/admin/find-snack-customers)
- [delete one or many customers of a given snack](https://gitlab.com/com.aven.snacker/snack-service/-/wikis/docs/snack-service/admin/delete-snack-customers)

As a user we can performs, throught the service, the following operations:

- find one or many snacks
- subscribe to a snack
- unsubscribe to a snack
- edit owned snack (Owner)
- remove owned snack (Owner)



