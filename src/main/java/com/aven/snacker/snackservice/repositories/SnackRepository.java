package com.aven.snacker.snackservice.repositories;

import com.aven.snacker.snackservice.core.snack.Snack;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.Set;

@Repository
public interface SnackRepository extends ReactiveCrudRepository<Snack, String> {
     Mono<Boolean> existsByOwner(String owner);
     Mono<Boolean> existsByPhoneNumber(long phone);
     Mono<Snack> findByPhoneNumber(long phone);
     Mono<Boolean> existsByEmail(String email);
     Mono<Boolean> existsByEmailOrPhoneNumber(String email,long phoneNumber);
     Mono<Snack> findByEmail(String email);
     Flux<Snack> findByOwner(String owner);
     Flux<Snack> findAllByName(String name);
     Mono<Void> deleteByPhoneNumber(long phoneNumber);
     Mono<Void> deleteByEmail(String phoneNumber);
     Mono<Void> deleteByOwner(String owner);
     Mono<Void> deleteAllById(Collection<String> ids);
     @Query(value = "{_id:{$exists:true}}",fields = "{'customers':0,'subscriptions':0,'invitations':0,'deleted':0,'accessCode':0}")
     Flux<Snack> findAllSnacks(Pageable pageable);
     @Query(value = "{?0:?1,_id:{$exists:true}}")
     Flux<Snack> findAllSnacksWithFilter(Pageable pageable,String filter,String filterValue);
     @Override
     Flux<Snack> findAll();
}
