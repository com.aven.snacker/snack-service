package com.aven.snacker.snackservice.controllers;

import com.aven.snacker.snackservice.Services.CustomerService;
import com.aven.snacker.snackservice.Services.SnackService;
import com.aven.snacker.snackservice.Services.SubscriptionService;
import com.aven.snacker.snackservice.core.exceptions.InvalidRequestParameterException;
import com.aven.snacker.snackservice.core.exceptions.RestrictedOperationException;
import com.aven.snacker.snackservice.core.evaluations.Rate;
import com.aven.snacker.snackservice.core.users.Customer;
import com.aven.snacker.snackservice.core.users.subscriptions.Subscription;
import com.mongodb.client.result.UpdateResult;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Set;

@RestController
@RequestMapping("snacker/snack-service/")
public class CustomerController {
    private final Logger logger= LoggerFactory.getLogger(SnackController.class);
    private final Environment env;
    private final SnackService snackService;
    private final SubscriptionService subscriptionService;
    private final CustomerService customerService;
@Autowired
    public CustomerController(Environment env, SnackService snackService, SubscriptionService subscriptionService, CustomerService customerService) {
        this.env = env;
        this.snackService = snackService;
        this.subscriptionService = subscriptionService;
    this.customerService = customerService;
}

    /**
     * This method is an endpoint for subscribing to a snack
     * @param auth the authenticated user id
     * @param snackId snack id
     * @return a void as Mono
     */
    @PostMapping("snacks/{snackId}/subscriptions")
    public Mono<Void> subscribe(@AuthenticationPrincipal Jwt auth, @PathVariable() String snackId){
        verifyId(snackId);
        return subscriptionService.addSubscription(snackId,auth.getSubject());
    }
    @PostMapping("snacks/{snackId}/invitations/accept")
    public Mono<Void> acceptSubscription(@AuthenticationPrincipal Jwt auth,@RequestParam(name = "accept") boolean accept, @PathVariable() String snackId){
        if (accept){
            return subscriptionService.isInvited(snackId, auth.getSubject()).flatMap(isInvited->{
                if (isInvited){
                    return customerService.addNewCustomer(snackId,new Customer(auth.getSubject(),null));
                }
                else
                    return Mono.error(new RestrictedOperationException(env.getProperty("CustomerController.acceptInvitation.notFound")));
            }).then(customerService.removeInvitation(snackId, auth.getSubject()));
        }
        else
            return customerService.removeInvitation(snackId,auth.getSubject());
    }

    /**
     * This method is an endpoint for loading all subscriptions
     * @param snackId snack id number
     * @return list of subscription as Flux
     */
    @GetMapping("snacks/{snackId}/subscriptions")
    public Flux<Subscription> getAllSubscription(@PathVariable(required = true) String snackId){
        verifyId(snackId);
        return subscriptionService.loadSubscriptions(snackId);
    }

    @GetMapping("snacks/{snackId}/invitations")
    public Flux<String> getAllInvitations(@PathVariable(required = true) String snackId){
        verifyId(snackId);
        return subscriptionService.loadInvitations(snackId);
    }
    /**
     * This method is an endpoint for removing a subscription
     * @param snackId
     * @param userId
     * @return a Void as Mono
     */
    @DeleteMapping("snacks/{snackId}/subscriptions/{userId}")
    public Mono<Void> delete(@PathVariable(required = true) String snackId,@PathVariable(required = true) String userId){
        verifyId(snackId);
        return subscriptionService.deleteSubscription(snackId,userId.trim());
    }
    @DeleteMapping("snacks/{snackId}/subscriptions")
    public Mono<Void> delete(@PathVariable(required = true) String snackId,@RequestBody(required = true) Set<String> userIds){
        verifyId(snackId);
        return subscriptionService.deleteSubscriptions(snackId,userIds);
    }

    @PatchMapping("snacks/{snackId}/subscriptions/{userId}")
    public Mono<Void> handleSubscription(@PathVariable String snackId,@PathVariable String userId,@RequestParam Boolean accept){
        verifyId(snackId);
        return subscriptionService.subscriptionHandler(snackId.trim(),userId,accept);
    }

    @GetMapping(path = "snacks/{snackId}/customers")
    public Flux<Customer> loadCustomers(@PathVariable String snackId){
        verifyId(snackId);
        return customerService.findCustomers(snackId);
    }
    @GetMapping(path = "snacks/{snackId}/customers/{userId}")
    public Mono<Customer> loadCustomer(@PathVariable String snackId,@PathVariable String userId){
        verifyId(snackId);
        return customerService.findCustomer(snackId,userId);
    }
    @DeleteMapping(path = "snacks/{snackId}/customers/{userId}")
    public Mono<Void> deleteCustomer(@PathVariable String snackId,@PathVariable String userId){
        verifyId(snackId);
        return customerService.deleteCustomer(snackId,userId);
    }
    @DeleteMapping(path = "snacks/{snackId}/customers/unsubscribe")
    public Mono<Void> unsubscribe(@PathVariable String snackId,@AuthenticationPrincipal Jwt auth){
        verifyId(snackId);
        return customerService.deleteCustomer(snackId, auth.getSubject());
    }
    @DeleteMapping(path = "snacks/{snackId}/subscriptions/remove-subscription")
    public Mono<Void> removeSubscription(@PathVariable String snackId,@AuthenticationPrincipal Jwt auth){
        verifyId(snackId);
        return subscriptionService.deleteSubscription(snackId, auth.getSubject());
    }
    @DeleteMapping(path = "snacks/{snackId}/customers/all")
    public Mono<Void> deleteCustomers(@PathVariable String snackId,@RequestBody Set<String> ids){
        verifyId(snackId);
        return customerService.deleteCustomers(snackId,ids);
    }
    @PostMapping(path = "snacks/{snackId}/rate")
    public Mono<Void> rate(@AuthenticationPrincipal Jwt auth, @PathVariable String snackId, @RequestBody() Rate rate){
        verifyId(snackId);
        return customerService.rate(snackId,auth.getSubject(),rate);
    }

    @GetMapping("test")
    public Mono<UpdateResult> test(){
        return customerService.incrementCustomerCount("5f4faf84d09a1136449557d5");
    }
    private void verifyId(String id){
        if (!ObjectId.isValid(id)){
            throw new InvalidRequestParameterException(env.getProperty("SnackController.invalidId"));
        }
    }

    @PostMapping(path = "snacks/invitations")
    public Mono<Void> sendInvitations(@RequestBody Set<String> users,@AuthenticationPrincipal Jwt auth){
        if (!users.isEmpty()){
            return customerService.invite(users,auth.getSubject());
        }
        throw  new InvalidRequestParameterException(env.getProperty("CustomerController.sendSimpleInvitation"));
    }

}
