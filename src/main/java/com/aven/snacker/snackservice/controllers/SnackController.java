package com.aven.snacker.snackservice.controllers;

import com.aven.snacker.snackservice.Services.CustomerService;
import com.aven.snacker.snackservice.Services.SnackService;
import com.aven.snacker.snackservice.core.exceptions.InvalidCountryCodeException;
import com.aven.snacker.snackservice.core.exceptions.InvalidRequestParameterException;
import com.aven.snacker.snackservice.core.exceptions.NoSnackAttributedException;
import com.aven.snacker.snackservice.core.utils.ElementsPage;
import com.aven.snacker.snackservice.core.utils.IsoCountriesUtil;
import com.aven.snacker.snackservice.core.utils.RandomString;
import com.aven.snacker.snackservice.core.snack.AccessCode;
import com.aven.snacker.snackservice.core.snack.Snack;
import com.aven.snacker.snackservice.core.users.BasicUserDetails;
import org.bson.types.ObjectId;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.*;

import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("snacker/snack-service/")
public class SnackController {
    private final Logger logger= LoggerFactory.getLogger(SnackController.class);
    private final Environment env;
    private final SnackService snackService;
    private final Sinks.One<Snack> newSnackSink;
    private final CustomerService customerService;

    @Autowired
    public SnackController(Environment env, SnackService snackService, @Qualifier("new-snack-processor") Sinks.One<Snack> newSnackSink, CustomerService customerService) {
        this.env = env;
        this.snackService = snackService;
        this.newSnackSink = newSnackSink;
        this.customerService = customerService;
    }

    /**
     * this methode is an endpoint to snack creation
     * @param snack
     * @param principal
     * @return a snack Object as Mono
     */
    @PostMapping(path = "snacks", consumes = "application/json", produces = "application/json")
    public Mono<ResponseEntity> createSnack(@RequestBody(required = true) @Valid Snack snack, @AuthenticationPrincipal Jwt principal){
        validateCountryCode(snack.getCountryCode());
        //initialization
        snack.initialize();
        BasicUserDetails user=extractUser(principal);
        snack.setOwner(principal.getSubject());
        snack.setPhoneNumber(user.getPhoneNumber());
        return snackService.save(snack).map(snack1 -> {
            newSnackSink.emitValue(snack1);
           return ResponseEntity.created(URI.create("snacker/snack-service/snacks/"+snack1.getId())).build();
        });
    }



    /**
     * this method provide an endpoint for finding a snack depending on its name or the associated phone
     * number or the owner id or depending on its id
     * @param id
     * @param phone
     * @param userId
     * @param name
     * @return a Snack(Mono) or suit(Flux) of snack
     */
    @GetMapping(path = "snacks",produces = "application/json")
    public Mono<Snack> findSnack(@RequestParam(name = "id",required = false) String id,
                                      @RequestParam(name = "phone",required = false) String phone,
                                      @RequestParam(name = "owner",required = false) String userId,
                                      @RequestParam(name = "name",required = false) String name

                                      ) {
        RequestParametersKeyValue<?> params= snackAccessParametersProcessing(id, phone, userId,name);
        if (params.key.equals(RequestParametersKey.PHONE))
            return snackService.findByPhoneNumber((Long) params.value);
        else if (params.key.equals(RequestParametersKey.ID))
            return snackService.findById((String) params.value);
        else if(params.key.equals(RequestParametersKey.USER_ID))
            return snackService.findByOwner((String) params.value);
        else
            throw new InvalidRequestParameterException(env.getProperty("PublicRestController.findOne.NoArgument"));
    }
    @GetMapping(path = "snacks/findAll",produces = "application/json")
    public Mono<ElementsPage<Snack>> findAllSnacks(@RequestParam(name = "limit",required = false) Integer limit,
                                            @RequestParam(name = "offset",required = false) Integer offset,
                                            @RequestParam(name = "filterBy",required = false) String filter,
                                            @RequestParam(name = "value",required = false) String value,
                                            @RequestParam(name = "sortBy",required = false) String sortBy
    ) {
        if (limit==null){
            limit=1;
        }
        if (offset==null){
            offset=0;
        }
        if (filter!=null&&filter.trim().isEmpty()){
            filter=null;
        }
        if (value!=null&&value.trim().isEmpty()){
            value=null;
        }
        if (sortBy!=null&&sortBy.trim().isEmpty()){
            sortBy=null;
        }

        return snackService.findAll(limit,offset,filter,value,sortBy);
    }


    /**
     * this method provide an endpoint for deleting a snack temporarily depending on or the associated phone
     * number or the owner id or depending on it id
     * @param id
     * @param phone
     * @param userId
     * @return a Void
     */

    @DeleteMapping(path = "snacks",produces = "application/json")
    public Mono<Void> deleteSnackTemporarily(@RequestParam(name = "id",required = false) String id,
                                             @RequestParam(name = "phone",required = false) String phone,
                                             @RequestParam(name = "owner",required = false) String userId,
                                             @RequestBody(required = false) String reason
                                 ) {
        RequestParametersKeyValue<?> params= snackAccessParametersProcessing(id, phone, userId,null);
        if (params.key.equals(RequestParametersKey.PHONE)){
            return snackService.deleteByPhoneNumber((Long) params.value,reason);
        }
        else if (params.key.equals(RequestParametersKey.ID)){
            return snackService.deleteById((String) params.value,reason);
        }
        else if (params.key.equals(RequestParametersKey.USER_ID))
            return snackService.deleteByOwner((String) params.value,reason);
        else throw new InvalidRequestParameterException(env.getProperty("PublicRestController.findOne.NoArgument"));
    }

    /**
     * this Rest endpoint is used to remove permanently a snack
     * @param id snack's id (not required)
     * @param phone user phone number (not required)
     * @param userId owner id (not required)
     * @return void
     */
    @DeleteMapping(path = "snacks/remove",produces = "application/json")
    public Mono<Void> deleteSnackPermanently(@RequestParam(name = "id",required = false) String id,
                                            @RequestParam(name = "phone",required = false) String phone,
                                            @RequestParam(name = "owner",required = false) String userId
    ) {
        RequestParametersKeyValue<?> params= snackAccessParametersProcessing(id, phone, userId,null);
        if (params.key.equals(RequestParametersKey.PHONE)){
            return snackService.hardDeleteByPhoneNumber((Long) params.value);
        }
        else if (params.key.equals(RequestParametersKey.ID)){
            return snackService.hardDeleteById((String) params.value);
        }
        else if (params.key.equals(RequestParametersKey.USER_ID))
            return snackService.hardDeleteByOwner((String) params.value);
        else throw new IllegalArgumentException(env.getProperty("PublicRestController.findOne.NoArgument"));
    }

    @DeleteMapping(path = "snacks/remove/all",produces = "application/json")
    public Mono<Void> deleteSnacksPermanently(@RequestBody Set<String> snackIds
                                              ) {
        Set<String> ids= snackIds.stream().filter(ObjectId::isValid)
                .collect(Collectors.toSet());
        return snackService.deleteSnacks(ids);
    }



    @GetMapping(path = "snacks/access-code", produces = "application/json")
    public Mono<AccessCode> getAccessCode(@AuthenticationPrincipal Jwt principal){
        BasicUserDetails user= extractUser(principal);
        if (!user.isSnack){
            throw new NoSnackAttributedException(env.getProperty("snackController.getAccessCode.noSnackAssociated"));
        }
        AccessCode accessCode=new AccessCode(new RandomString().nextString(), LocalDateTime.now());
            return snackService.newAccessCode(accessCode,user.getUsername())
                    .thenReturn(accessCode);
    }

    /**
     * this method is an endpoint used to perform an update of  snack basic information
     * @param info
     * @param snackId
     * @return A Void as Mono
     */
    @PutMapping(path = "snacks/{snackId}", consumes = "application/json")
    public Mono<Void> updateSnackInformation(@RequestBody(required = true) Map<String,Object> info,@PathVariable(required = true) String snackId){
        if (!ObjectId.isValid(snackId.trim())) {
            throw new InvalidRequestParameterException(env.getProperty("PublicRestController.findOne.invalidSnackId"));
        }
        if (info!=null){
            return snackService.existsById(snackId)
            .then(snackService.updateInfo(info,snackId.trim()));
        }
        throw new InvalidRequestParameterException(env.getProperty("SnackController.updateInfo.invalidPhoneNumber"));
    }


    /**
     * This is an endpoint through which we can enable or disable a snack
     * @param enable true for enable or false for disable
     * @param isNew true if the snack is new
     * @param snackId the id of the snack into which perform operation
     * @return A void as Mono
     */
    @PatchMapping(path = "snacks/{snackId}/change-state")
    public Mono<Void> changeState(@RequestParam(name = "enable",required = false) String enable,@PathVariable String snackId,@RequestParam(name = "isNew",required = false) String isNew){
        if (!ObjectId.isValid(snackId.trim())) {
            throw new InvalidRequestParameterException(env.getProperty("PublicRestController.findOne.invalidSnackId"));
        }
        if (enable!=null&&!enable.trim().isEmpty()){
            validateBoolean(enable);
            return snackService.enableState(Boolean.parseBoolean(enable),snackId);
        }
        else if (isNew!=null&&!isNew.trim().isEmpty()){
            validateBoolean(isNew);
            return snackService.isNew(Boolean.parseBoolean(isNew),snackId);
        }
        throw new InvalidRequestParameterException(env.getProperty("validation.default"));
    }

    /**
     * This method is used to restore a snack after it's been temporarily deleted
     * @param userId the owner id
     * @return A Void as a mono
     */
    @PutMapping(path = "snacks/backup")
    public Mono<Void> backUpSnack(@RequestParam(name = "owner",required = true) String userId){
        return snackService.backup(userId);
    }

    /**
     * This method is an entrypoint to get snack information for a web or mobile app initialization
     * @param auth current user information as jwt from the bearer token
     * @return stream of snack
     */
    @GetMapping(path = "snacks/connect")
    public Stream<Snack> InitializeSnackConnection(@AuthenticationPrincipal Jwt auth){
        return snackService.initSnackConnection(auth.getSubject()).toStream();
    }













    private RequestParametersKeyValue<?> snackAccessParametersProcessing(String id,String phone,String userId,String name){
          if (phone != null && !phone.trim().isEmpty()) {
            try {
                return new RequestParametersKeyValue<Long>(RequestParametersKey.PHONE, Long.parseLong(phone));
            } catch (NumberFormatException e) {
                throw new InvalidRequestParameterException(env.getProperty("PublicRestController.findOne.invalidPhoneNumber"));
            }
        }
        if (id != null && !id.trim().isEmpty()) {
            if (!ObjectId.isValid(id.trim())) {
                throw new InvalidRequestParameterException(env.getProperty("PublicRestController.findOne.invalidSnackId"));
            }
            return new RequestParametersKeyValue<String>(RequestParametersKey.ID, id);
        }
        if (userId != null && !userId.trim().isEmpty()) {

            return new RequestParametersKeyValue<String>(RequestParametersKey.USER_ID, userId);
        }
        if (name != null && !name.trim().isEmpty()) {

            return new RequestParametersKeyValue<String>(RequestParametersKey.NAME, name);
        }
        return new RequestParametersKeyValue(RequestParametersKey.NONE, null);
    }


    private BasicUserDetails extractUser(Jwt jwt){
        Assert.notNull(jwt.getClaimAsString("preferred_username"), env.getProperty("PublicRestController.extractUser.nullSubject"));
        BasicUserDetails user=new BasicUserDetails();
        user.setUsername(jwt.getClaimAsString("preferred_username"));
        user.setEmail(jwt.getClaimAsString("email"));
        user.setFirstName(jwt.getClaimAsString("given_name"));
        user.setLastName(jwt.getClaimAsString("family_name"));
        user.setPhoneNumber(Long.parseLong(jwt.getClaimAsString("phone_number")));
        user.setSnackId(jwt.getClaimAsString("snack_id"));
        return user;
    }

    private static class RequestParametersKeyValue<T>{
        public RequestParametersKeyValue(RequestParametersKey key, T value) {
            this.key = key;
            this.value = value;
        }

        RequestParametersKey key;
        T value;
    }

    private void validateCountryCode(String code){
        if (!IsoCountriesUtil.isValidCountryCode(code)){
            throw new InvalidCountryCodeException(env.getProperty("PublicRestController.createNewSnack.invalidCountryCode"));
        }
    }
    private void validateBoolean(String value){
        if (value!=null
                &&!value.equalsIgnoreCase("true")
                &&!value.equalsIgnoreCase("false")){
            throw new InvalidRequestParameterException(env.getProperty("SnackController.changeSnackState.invalidState"));
        }
    }
}
