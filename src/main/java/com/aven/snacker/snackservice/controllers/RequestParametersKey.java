package com.aven.snacker.snackservice.controllers;

public enum RequestParametersKey {
    PHONE("phone"),EMAIL("email"),ID("id"),USER_ID("userId"),NAME("name"),NONE("none");
    final String key;
    RequestParametersKey(String key){
        this.key=key;
    }
    public String getKey(){
        return key;
    }
}
