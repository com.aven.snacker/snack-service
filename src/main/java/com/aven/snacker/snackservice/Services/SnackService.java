package com.aven.snacker.snackservice.Services;

import com.aven.snacker.snackservice.controllers.RequestParametersKey;
import com.aven.snacker.snackservice.controllers.SnackController;
import com.aven.snacker.snackservice.core.exceptions.*;
import com.aven.snacker.snackservice.core.utils.ElementsPage;
import com.aven.snacker.snackservice.core.utils.IsoCountriesUtil;
import com.aven.snacker.snackservice.core.utils.Validation;
import com.aven.snacker.snackservice.core.evaluations.Rate;
import com.aven.snacker.snackservice.core.snack.AccessCode;
import com.aven.snacker.snackservice.core.snack.Snack;
import com.aven.snacker.snackservice.core.users.BasicUserDetails;
import com.aven.snacker.snackservice.repositories.SnackRepository;
import com.mongodb.client.result.UpdateResult;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@Service
public class SnackService {
    private final Environment env;
    private final SnackRepository repository;
    private final ReactiveMongoTemplate template;
    private final Sinks.One<Map<String,String>> sink;
    private final Sinks.One<String> backupSink;
    private Logger logger= LoggerFactory.getLogger(SnackController.class);
    private final NotificationService notificationService;
    @Autowired
    public SnackService(Environment env, SnackRepository repository, ReactiveMongoTemplate template, @Qualifier("snack-deleted-processor") Sinks.One<Map<String, String>> sink, @Qualifier("snack-backup-processor") Sinks.One<String> backupSink, NotificationService notificationService) {
        this.env = env;
        this.repository = repository;
        this.template = template;
        this.sink = sink;
        this.backupSink = backupSink;
        this.notificationService = notificationService;
    }



    /* ************************ SAVE ******************************* */


    /**
     * This method is used to create a new snack
     * @param snack
     * @return a snack Object as a mono
     */
    public Mono<Snack> save(Snack snack){
        Assert.notNull(snack, env.getProperty("SnackService.save.nullSnackArgument"));
        return repository.findByOwner(snack.getOwner())
                .handle((b,sink)->{
                    if (b!=null){
                        if (b.isDeleted()){
                            sink.error(new DeletedSnackException(env.getProperty("SnackService.find.snackDeleted")));
                        }
                        else sink.error(new DuplicateEntityException(env.getProperty("SnackService.save.duplicateSnack")));
                    }
                })
                .then(repository.save(snack))
                .onErrorMap(throwable -> {
                    if (throwable.getClass()!=DuplicateEntityException.class&&throwable.getClass()!=DeletedSnackException.class){
                        throwable.printStackTrace();
                        throw new  RuntimeException(env.getProperty("SnackService.save.savingError"));
                    }
                    else return throwable;
                })
                ;
    }



    /* ************************ SEARCH - FLUSH ******************************* */


    /**
     * This methode is used to retrieve all snacks from the database
     * @return list of snack as Flux
     */
    public Mono<ElementsPage<Snack>> findAll(int limit, int offset, String filter, String value, String sortBy){
        Query query=new Query();
        ElementsPage<Snack> page= new ElementsPage<>();
        query.fields().exclude("customers")
                .exclude("subscriptions")
                .exclude("lastUpdate")
                .exclude("invitations");
            return repository.count()
                    .map(count->{
                        page.setTotal(count);
                        return count;
                    }).flatMapMany(c->{
                        if (filter!=null&&value!=null){
                        return repository.findAllSnacksWithFilter(
                            PageRequest.of(offset,limit,sortBy!=null?Sort.by(sortBy):Sort.by("name")),
                            filter,
                            value);
                        }
                        else
                             return repository.findAllSnacks(
                                PageRequest.of(offset,limit,sortBy!=null?Sort.by(sortBy):Sort.by("name"))
                    );

                    }).collectList().map(snacks -> {
                        page.setContent(snacks);
                        return page;
                    });
    }


    /**
     * This method is used to retrieve one snack depending on his id
     * @param id
     * @return a Snack Object as Mono
     */
    public Mono<Snack> findById(@NotNull String id){
        return
                template.findOne(getFindSnackQuery(new KeyValuePair("_id",id)),Snack.class)
                        .map(snack -> {
                            isDeletedOrDesabled(snack);
                            return snack;
                        })
                        .onErrorMap(throwable -> {
                            if (throwable instanceof DeletedSnackException
                            || throwable instanceof DisabledSnackException
                            ){
                                return throwable;
                            }
                            throwable.printStackTrace();
                            return new  RuntimeException(env.getProperty("SnackService.find.error"));
                        })

                ;
    }


    /**
     * This method is used to retrieve one snack depending on the configured phone number
     * @param phone
     * @return a Snack Object as Mono
     */
    public Mono<Snack> findByPhoneNumber(long phone){

        return template.findOne(getFindSnackQuery(new KeyValuePair("phoneNumber",phone)),Snack.class)
                .doOnSuccess(this::isDeletedOrDesabled)
                .onErrorMap(throwable -> {
                    if (throwable instanceof DeletedSnackException
                            || throwable instanceof DisabledSnackException
                    ){
                        return throwable;
                    }
                    throwable.printStackTrace();
                    return new  RuntimeException(env.getProperty("SnackService.find.error"));
                })
                ;
    }


    /**
     * This method is used to retrieve one snack depending on the configured email address
     * @param email
     * @return a Snack Object as Mono
     */
    public Mono<Snack> findByEmail(String email){

        return template.findOne(getFindSnackQuery(new KeyValuePair("email",email)),Snack.class)
                .doOnSuccess((snack) -> {
                    if (snack.isDeleted()){
                        throw new DeletedSnackException(env.getProperty("SnackService.find.snackDeleted"));
                    }
                })
                .onErrorMap(throwable -> {
                    if (throwable instanceof DeletedSnackException){
                        return throwable;
                    }
                    return new  RuntimeException(env.getProperty("SnackService.find.error"));
                })
                ;
    }


    /**
     * This method is used to retrieve one snack depending on the owner id
     * @param userId
     * @return a Snack Object as Mono
     */
    public Mono<Snack> findByOwner(String userId){

        return template.findOne(getFindSnackQuery(new KeyValuePair("owner",userId)),Snack.class)
                .onErrorMap(throwable -> new  RuntimeException(env.getProperty("SnackService.find.error")))

                ;
    }


    /**
     * This method is used to retrieve one snack depending on it's name
     * @param name
     * @return a Snack Object as Mono
     */
    public Flux<Snack> findByName(String name){
        return template.find(getFindSnackQuery(new KeyValuePair("name",name)),Snack.class)
                .filter(snack -> !snack.isDeleted())
                .filter(Snack::isEnabled)
                .onErrorMap(throwable -> {
            if (throwable instanceof DeletedSnackException
                    || throwable instanceof DisabledSnackException
            ){
                return throwable;
            }
                    throwable.printStackTrace();
            return new  RuntimeException(env.getProperty("SnackService.find.error"));
        })
                ;
    }




    /* ************************ DELETE ******************************* */
    /**
     * This method is used to delete a snack permanently depending on his id
     * @param id
     * @return a void as Mono
     */
    public Mono<Void> hardDeleteById(String id){
         return repository.deleteById(id)
                 .onErrorMap(throwable -> new  RuntimeException(env.getProperty("SnackService.delete.error")));
    }
    /**
     * This method is used to delete a snack permanently depending on the owner id
     * @param owner
     * @return a void as Mono
     */
    public Mono<Void> hardDeleteByOwner(String owner){
        return repository.deleteByOwner(owner)
                .onErrorMap(throwable -> new  RuntimeException(env.getProperty("SnackService.delete.error")));
    }
    /**
     * This method is used to delete a snack permanently depending on the configured phoneNumber
     * @param phone
     * @return a void as Mono
     */
    public Mono<Void> hardDeleteByPhoneNumber(long phone){
        return repository.deleteByPhoneNumber(phone)
                .onErrorMap(throwable -> new  RuntimeException(env.getProperty("SnackService.delete.error")));
    }

    /**
     * This method is used to delete a snack depending on his id
     * @param id
     * @param reason the reason why owner would remove the snack
     * @return a void as Mono
     */
    public Mono<Void> deleteById(String id,@Nullable String reason){

        return existsById(id)
                .then(deleteTemporarily(RequestParametersKey.ID,id.toString(),reason))
                .onErrorMap(throwable ->{
                    throwable.printStackTrace();
                    if (throwable instanceof ResourceNotFoundException){
                        return throwable;
                    }

                    else return new  RuntimeException(env.getProperty("SnackService.delete.error"));
                })
                ;
    }



    /**
     * This method is used to delete a snack depending on the configured phone number
     * @param phone
     * @param reason the reason why owner would remove the snack
     * @return a void as Mono
     */
    public Mono<Void> deleteByPhoneNumber(long phone,String reason){

        return existsByPhoneNumber(phone)
                .then(deleteTemporarily(RequestParametersKey.PHONE,String.valueOf(phone),reason))
                .onErrorMap(throwable ->{
                    if (throwable instanceof ResourceNotFoundException){
                        return throwable;
                    }
                    else return new  RuntimeException(env.getProperty("SnackService.delete.error"));
                })
                ;
    }


    /**
     * This method is used to delete a snack depending the configured email
     * @param userId
     * @param reason the reason why owner would remove the snack
     * @return a void as Mono
     */
    public Mono<Void> deleteByOwner(String userId,@Nullable String reason){

        return existsByOwner(userId)
                .then(deleteTemporarily(RequestParametersKey.USER_ID,userId,reason))
                .onErrorMap(throwable ->{
                    if (throwable instanceof ResourceNotFoundException){
                        return throwable;
                    }
                    else return new  RuntimeException(env.getProperty("SnackService.delete.error"));
                })
                ;
    }


    public Mono<Void> deleteSnacks(Set<String> ids){
        return repository.deleteAllById(ids);
    }


    /* ************************ VERIFICATION ******************************* */


    /**
     * This method is used to verifie if a snack with the given email address exists
     * @param email
     *
     * @return a Boolean as Mono
     */
    private Mono<Boolean> existsByEmail(String email) {
        return repository.existsByEmail(email)
                .handle((b, snackSynchronousSink) -> {
                    if (!b){
                        snackSynchronousSink.error(new ResourceNotFoundException(env.getProperty("SnackService.find.NotFound")));
                    }
                })
                ;
    }


    /**
     * This method is used to verifie if a snack with the given phone number exists
     * @param phone
     * @return a Boolean as Mono
     */
    private Mono<Boolean> existsByPhoneNumber(long phone) {
        return repository.existsByPhoneNumber(phone)
                .handle((b, snackSynchronousSink) -> {
                    if (!b){
                        snackSynchronousSink.error(new ResourceNotFoundException(env.getProperty("SnackService.find.NotFound")));
                    }
                })
                ;
    }



    /**
     * This method is used to verifie if a snack with the given id exists
     * @param id
     * @return a Boolean as Mono
     */
    public Mono<Boolean> existsById(@NotNull String id) {
        return repository.existsById(id)
                .handle((b, snackSynchronousSink) -> {
                    if (!b){
                        snackSynchronousSink.error(new ResourceNotFoundException(env.getProperty("SnackService.find.NotFound")));
                    }
                })
                ;
    }



    /**
     * This method is used to verifie if a snack with the given id exists
     * @param userId
     * @return a Boolean as Mono
     */
    public Mono<Boolean> existsByOwner(String userId) {
        return repository.existsByOwner(userId)
                .handle((b, snackSynchronousSink) -> {
                    if (!b){
                        snackSynchronousSink.error(new ResourceNotFoundException(env.getProperty("SnackService.find.NotFound")));
                    }
                })
                ;
    }



    public Mono<Void> newAccessCode(AccessCode accessCode,String username){
        Assert.notNull(accessCode,env.getProperty("SnackService.newAccessCode.nullAccessCode"));
        Update update=new Update();
        update.set("accessCode",accessCode);
        Query query=new Query();
        query.addCriteria(Criteria.where("owner").is(username));
        return template.updateFirst(query,update,Snack.class).handle((updateResult, sink) -> {
            if (!updateResult.wasAcknowledged()){
                sink.error(new RuntimeException(env.getProperty("SnackService.newAccessCode.notUpdated")));
            }
        });
    }


    /* ************************ UPDATES ******************************* */


    /**
     * This method is used to update snack's basic informations
     * @param info
     * @param snackId
     * @return
     */
    public Mono<Void> updateInfo(Map<String,Object> info,String snackId){
        Update update=new Update();
        for (String key:info.keySet()
             ) {
            Object value = info.get(key);
            if (value != null) {
                switch (key) {
                    case "name": update.set("name",value);
                    break;
                    case "phone_number":update.set("phoneNumber",value);
                    break;
                    case "email":{
                        if (!Validation.email((String) value)){
                            throw new InvalidCountryCodeException(env.getProperty("Validation.invalidEmail"));
                        }
                        update.set("email",value);
                    }
                    break;
                    case "country_code":{
                        if (!IsoCountriesUtil.isValidCountryCode((String) value)){
                            throw new InvalidCountryCodeException(env.getProperty("Validation.invalidCountryCode"));
                        }
                        update.set("countryCode",value);
                    };
                    break;
                    case "region_code":update.set("regionCode",value);
                    break;
                    case "address":update.set("address",value);
                    break;
                }
            }
        }
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(snackId));
        return template.updateFirst(query,update,Snack.class)
                .onErrorMap(throwable -> new RuntimeException(env.getProperty("SnackService.update.updateError")))
                .then();

    }

    /**
     * This method is triggered when a user is updated so that his informations are updated in the snacks
     * @param user
     * @return
     */
    public Flux<Snack> updateSnackUserInfo(BasicUserDetails user){
        Assert.notNull(user, env.getProperty("SnackService.update.nullUserArgument"));
        Update update=new Update();
        update.set("email",user.getEmail());
        update.set("phoneNumber",user.getPhoneNumber());
        Query query=new Query();
        query.addCriteria(Criteria.where("owner").is(user.getUsername()));
        return template.updateFirst(query,update,Snack.class)
                .thenMany(repository.findByOwner(user.getUsername()))
                .map(snack -> {
                    return snack;
                })
                .onErrorMap(throwable -> new  RuntimeException(env.getProperty("SnackService.update.updateError")));
    }

    /**
     * This method is used to enable or disable a snack
     * @param enabled
     * @param id
     * @return a void as Mono
     */
    public Mono<Void> enableState(boolean enabled,String id){
        env.getProperty("SnackService.changeSnackState.invalidState");
        if (id==null || !ObjectId.isValid(id)){
            throw new IllegalArgumentException(env.getProperty("SnackService.changeSnackState.invalidSnackId"));
        }
        return updateSnack(RequestParametersKey.ID,id,new KeyValuePair("enabled",enabled)).handle((result, sink1) -> {
            if (result.getMatchedCount()==0){
                sink1.error(new ResourceNotFoundException(env.getProperty("SnackService.find.NotFound")));
            }
        });
    }
    public Mono<Void> isNew(boolean isNew,String id){
        env.getProperty("SnackService.changeSnackState.invalidState");

        if (id==null || !ObjectId.isValid(id)){
            throw new IllegalArgumentException(env.getProperty("SnackService.changeSnackState.invalidSnackId"));
        }
        return updateSnack(RequestParametersKey.ID,id,new KeyValuePair("isNew",isNew)).handle((result, sink1) -> {
            if (result.getMatchedCount()==0){
                sink1.error(new ResourceNotFoundException(env.getProperty("EntityNotFound")));
            }
        });
    }


    public Mono<Void> backup(String owner){
        return updateSnack(RequestParametersKey.USER_ID,owner,new KeyValuePair("deleted",false),new KeyValuePair("deletionDate",null))
                .handle((result, sink1) -> {
                    if (result.getModifiedCount()==0){
                        sink1.error(new RuntimeException(env.getProperty("SnackService.backup.error")));
                    }
                })
                .doOnSuccess(o -> {
                    backupSink.emitValue(owner);
                })
                .then();

    }

    private Mono<Void> deleteTemporarily(RequestParametersKey indexKey,String indexValue,String reason ){
        return updateSnack(indexKey,indexValue,
                new KeyValuePair("deleted", true),
                new KeyValuePair("deletionDate",LocalDateTime.now()))
                .doOnSuccess(result -> {
                    System.out.println(result);
                    if (result.getModifiedCount()>0){

                        sink.emitValue(Map.of("index",indexKey.getKey(),"value",indexValue,"reason",reason==null?"null":reason));
                    }
                }).then();
    }

    private Mono<UpdateResult> updateSnack(RequestParametersKey indexKey, String indexValue , KeyValuePair ...updates){
        Update update=new Update();
        for (KeyValuePair upd: updates
             ) {
            update.set(upd.getKey(), upd.getValue());
        }
        Query query=new Query();
        if (indexKey.equals(RequestParametersKey.ID)){
            query.addCriteria(Criteria.where("_id").is(indexValue));
        }else if(indexKey.equals(RequestParametersKey.PHONE)){
            query.addCriteria(Criteria.where("phoneNumber").is(Long.parseLong(indexValue)));
        }else if (indexKey.equals(RequestParametersKey.USER_ID)){
            query.addCriteria(Criteria.where("owner").is(indexValue));
        }
        return template.updateFirst(query,update,Snack.class)
                .doOnSuccess(result -> {})
                .onErrorMap(throwable -> {
                    throwable.printStackTrace();
                    return new RuntimeException(env.getProperty("SnackService.update.updateError"));
                })
                ;
    }
    private static class KeyValuePair{
        private final String key;
        private final Object value;
        private KeyValuePair(String key, Object value) {

            this.key = key;
            this.value = value;
        }

        public Object getValue() {
            return value;
        }

        public String getKey() {
            return key;
        }
    }

    private void  isDeletedOrDesabled(Snack snack){
        if (snack.isDeleted()){
            throw new DeletedSnackException(env.getProperty("SnackService.find.snackDeleted"));
        }
        if (!snack.isEnabled()){
            throw new DisabledSnackException(env.getProperty("SnackService.find.snackDisabled"));
        }
    }
    public Flux<Snack> initSnackConnection(String owner){
        return repository.findByOwner(owner)
                .onErrorMap(throwable -> {
                    throwable.printStackTrace();
                    return new RuntimeException(env.getProperty("SnackService.init.error"));
                })
                ;
    }

    public Mono<Tuple2<Long,Float>> getSnackCountRate(String snackId){
        Assert.notNull(snackId,env.getProperty("NullArgumentValue"));
        Query query=new Query();
        query.fields().include("quote").include("customers");
        query.addCriteria(Criteria.where("_id").is(snackId));
        Query query2=new Query();
        return template.findOne(query,Snack.class)
                .map(snack ->  {
                    return Tuples.of(snack.getCustomers()
                            .stream()
                            .filter(customer -> customer.getRate()!=null)
                            .count(),
                            snack.getQuote());
                });

    }
    public Mono<Void> actualizeQuote(String id, Rate rate){
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        UnwindOperation unwindOperation=UnwindOperation.newUnwind().path("customers").noArrayIndex().skipNullAndEmptyArrays();
        final Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(Criteria.where("customers.rate").exists(true)),
                Aggregation.unwind("customers"),
                Aggregation.group().count().as("count"));
        AggregationUpdate update=AggregationUpdate.update()
                .set("quote").toValue(AccumulatorOperators.Avg.avgOf("$customers.rate.rate"));
       return template.updateFirst(query,update,Snack.class).then();
    }

    private Query getFindSnackQuery(KeyValuePair pair){
        Query query=new Query();
        query.addCriteria(Criteria.where(pair.key).is(pair.value));
        query.fields().exclude("customers")
                .exclude("subscriptions")
                .exclude("lastUpdate")
                .exclude("invitations");
        return query;
    }

}
