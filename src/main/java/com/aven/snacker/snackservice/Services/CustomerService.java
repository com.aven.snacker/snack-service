package com.aven.snacker.snackservice.Services;

import com.aven.snacker.snackservice.controllers.SnackController;
import com.aven.snacker.snackservice.core.exceptions.EventSendingException;
import com.aven.snacker.snackservice.core.exceptions.ResourceNotFoundException;
import com.aven.snacker.snackservice.core.users.invitations.DefaultInvitation;
import com.aven.snacker.snackservice.core.evaluations.Rate;
import com.aven.snacker.snackservice.cloud.messaging.EventDetails;
import com.aven.snacker.snackservice.core.users.Customer;
import com.aven.snacker.snackservice.core.snack.Snack;
import com.aven.snacker.snackservice.repositories.SnackRepository;
import com.mongodb.*;
import com.mongodb.client.result.UpdateResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

@Service
public class CustomerService {
    private final String CUSTOMER_FIELD="customers";
    private final Environment env;
    private final SnackRepository repository;
    private final ReactiveMongoTemplate template;
    private Logger logger= LoggerFactory.getLogger(SnackController.class);
    private final Sinks.One<Customer> customerSink;
    private final Sinks.One<DefaultInvitation> invitationSink;
    private final SnackService snackService;
    @Autowired
    public CustomerService(Environment env, SnackRepository repository, ReactiveMongoTemplate template, @Qualifier("new-customer-sink") Sinks.One<Customer> customerSink, Sinks.One<DefaultInvitation> invitationSink, SnackService snackService) {
        this.env = env;
        this.repository = repository;
        this.template = template;
        this.customerSink = customerSink;
        this.invitationSink = invitationSink;
        this.snackService = snackService;
    }
    public Mono<Void> addNewCustomer(String snackId,Customer customer){
        Assert.notNull(customer,env.getProperty("NullArgumentValue"));
        Update update=new Update();
        Query query=new Query();
        update.addToSet(CUSTOMER_FIELD,customer);
        query.addCriteria(Criteria.where("_id").is(snackId));
        return template.updateFirst(query,update, Snack.class)
                .map(result -> {
                    if (result.getModifiedCount()>0){
                        customerSink.emitValue(customer);
                    }
                    return result;
                }).and(incrementCustomerCount(snackId)).then();

    }
    /**
     * this method is used to retrieve all customers from a given snack
     * @param snackId the given snack id
     * @return list of snacks as Flux
     */
    public Flux<Customer> findCustomers(String snackId){
        Assert.notNull(snackId,env.getProperty("NullArgumentValue"));
        Query query=new Query();
        query.fields().include("customers");
        query.addCriteria(Criteria.where("_id").is(snackId));
        return assertExist(snackId).thenMany(template.find(query,Snack.class)).flatMapIterable(Snack::getCustomers);
    }
    public Mono<Customer> findCustomer(String id,String userId){
        Assert.notNull(id,env.getProperty("NullArgumentValue"));
        Assert.notNull(userId,env.getProperty("NullArgumentValue"));
        Query query=new Query();
        query.fields().include("customers");
        query.addCriteria(Criteria.where("_id").is(id));
        return assertExist(id).then(template.findOne(query,Snack.class))
                .flatMapIterable(Snack::getCustomers)
                .filter(customer -> customer.getUserId().equals(userId))
                .next()
                ;
    }

    public Mono<Void> deleteCustomer(String snackId,String userId){
        Assert.notNull(snackId,env.getProperty("NullArgumentValue"));
        Assert.notNull(userId,env.getProperty("NullArgumentValue"));
        assertExist(snackId);
        Update update=new Update();
        Query query=new Query();
        update.pull(CUSTOMER_FIELD,new Query(Criteria.where("userId").is(userId)));
        query.addCriteria(Criteria.where("_id").is(snackId));
        FindAndModifyOptions options=new FindAndModifyOptions();
        options.returnNew(false);
        return template.findAndModify(query,update,options,Snack.class).then();
    }
    public Mono<Void> deleteCustomers(String snackId, Set<String> userIds){
        Assert.notNull(snackId,env.getProperty("NullArgumentValue"));
        Assert.notNull(userIds,env.getProperty("NullArgumentValue"));
        assertExist(snackId.toString());
        Update update=new Update();
        Query query=new Query();
        update.pull(CUSTOMER_FIELD,new Query().addCriteria(Criteria.where("userId").in(userIds)));
        query.addCriteria(Criteria.where("_id").is(snackId));
        FindAndModifyOptions options=new FindAndModifyOptions();
        options.returnNew(false);
        return template.findAndModify(query,update,options,Snack.class).then()
                ;
    }

    public Mono<Void> rate(String id, String userId, Rate rate){
        rate.setDate(LocalDateTime.now());
        Assert.notNull(id,env.getProperty("NullArgumentValue"));
        Assert.notNull(userId,env.getProperty("NullArgumentValue"));
        Update update=new Update();
        Query query=new Query();
        query.fields().include(CUSTOMER_FIELD);
        query.addCriteria(Criteria.where("_id").is(id).and("customers.userId").is(userId));
        update.set("customers.$.rate",rate);
        return template.updateFirst(query,update,Snack.class)
                .handle((result, sink) -> {
                    if (result.getMatchedCount()==0){
                        sink.error(new ResourceNotFoundException(env.getProperty("CustomerController.rate.notFound")));
                    }
    }).then(snackService.actualizeQuote(id,rate));
    }

    public Mono<Void> assertExist(String snackId){
        return repository.existsById(snackId).handle((rs,sink)->{
            if (!rs){
                sink.error(new ResourceNotFoundException(env.getProperty("EntityNotFound")));
            }
        });
    }

    public Mono<UpdateResult> incrementCustomerCount(String id){
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        Update update=new Update();
        update.inc("customerCount");
        return template.updateFirst(query,update,Snack.class);

    }


    public Mono<Void> invite(Set<String> users, String userId) {
        Query query=new Query();
        query.addCriteria(Criteria.where("owner").is(userId));
        query.fields().include("_id");
        return template.findOne(query,Snack.class)
                .doOnSuccess(snack -> {
                    for (String id:users
                         ) {
                        sendSimpleInvitation(new DefaultInvitation(id,snack.getId(),new Date()));

                    }
                }).then(addInvitations(users,userId)).then();
    }
    private Mono<Void> addInvitations(Set<String> users, String userId){
        Query query=new Query();
        query.addCriteria(Criteria.where("owner").is(userId));
        query.fields().include("_id");
        Update update=new Update();
        update.addToSet("invitations",BasicDBObjectBuilder.start("$each",users).get());
        return template.findAndModify(query,update,Snack.class).then();
    }
    public Mono<Void> removeInvitation(String snackId,String userId){
        Query query = new Query();
        query.fields().include("invitations");
        query.addCriteria(Criteria.where("_id").is(snackId));
        Update update=new Update();
        update.pull("invitations",userId);
        return template.updateFirst(query,update,Snack.class).then();
    }
    public void sendSimpleInvitation(DefaultInvitation invitation){
       Sinks.Emission emission= invitationSink.emitValue(invitation);
        if (emission.hasFailed()&&!emission.wasPreviouslyTerminated()){
           throw new EventSendingException(env.getProperty("CustomerService.sendInvitation.sendingFailed"), new EventDetails("invitation",invitation,new Date()));
       }

    }
}
