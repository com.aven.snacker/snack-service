package com.aven.snacker.snackservice.Services;
import com.aven.snacker.snackservice.controllers.SnackController;
import com.aven.snacker.snackservice.core.exceptions.DuplicateEntityException;
import com.aven.snacker.snackservice.core.exceptions.ResourceNotFoundException;
import com.aven.snacker.snackservice.core.users.Customer;
import com.aven.snacker.snackservice.core.snack.Snack;
import com.aven.snacker.snackservice.core.users.subscriptions.SubscriptionDetails;
import com.aven.snacker.snackservice.core.users.subscriptions.Subscription;
import com.aven.snacker.snackservice.repositories.SnackRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import java.time.LocalDateTime;
import java.util.Set;

@Service
public class SubscriptionService {

    private final String SUBSCRIPTION_FIELD="subscriptions";
    private final String CUSTOMERS_FIELD="customers";
    private final Environment env;
    private final SnackRepository repository;
    private final ReactiveMongoTemplate template;
    private Logger logger= LoggerFactory.getLogger(SnackController.class);
    private final NotificationService notificationService;
    private final CustomerService customerService;
    private final Sinks.One<SubscriptionDetails> newSubscriptionSink;
    public SubscriptionService(Environment env,
                               SnackRepository repository,
                               ReactiveMongoTemplate template,
                               NotificationService notificationService,
                               CustomerService customerService,
                               @Qualifier("new-subscriber-sink") Sinks.One<SubscriptionDetails> newSubscriptionSink) {
        this.env = env;
        this.repository = repository;
        this.template = template;
        this.notificationService = notificationService;
        this.customerService = customerService;
        this.newSubscriptionSink = newSubscriptionSink;
    }

    /**
     * This methode is used for add new subscription in the database
     * @param snackId
     * @param userId
     * @return Void as Mono
     */
    public Mono<Void> addSubscription(String snackId,String userId){
        Assert.notNull(snackId,env.getProperty("NullArgumentValue"));
        Update update=new Update();
        Subscription subscription=new Subscription(LocalDateTime.now(),userId);
        update.addToSet("subscriptions",subscription);
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(snackId));
        return  isSubscriberOrCustomer(snackId,userId).handle((aBoolean, synchronousSink) -> {
            if (aBoolean){
                synchronousSink.error(new DuplicateEntityException(env.getProperty("SubscriptionService.addSubscription.alreadySubscribed")));
            }
        }).then(template.updateFirst(query,update, Snack.class)
                .handle((result,sink) -> {
                    if (result.getMatchedCount()==0){
                        sink.error(new ResourceNotFoundException(env.getProperty("SnackService.find.NotFound")));
                    }
                newSubscriptionSink.emitValue(new SubscriptionDetails(userId, snackId));
                }))
                ;
    }

    /**
     * This method is used to load all subscriptions of a given snack from the database
     * @param snackId the id of the snack from which to load subscriptions
     * @return a list of subscription as a flux
     */
    public Flux<Subscription> loadSubscriptions(String snackId){
        Assert.notNull(snackId,env.getProperty("NullArgumentValue"));
        Query query=new Query();
        query.fields().include(SUBSCRIPTION_FIELD);
        query.addCriteria(Criteria.where("_id").is(snackId));
        return template.findOne(query,Snack.class)
                .flatMapIterable(Snack::getSubscriptions);
    }
    public Flux<String> loadInvitations(String snackId){
        Assert.notNull(snackId,env.getProperty("NullArgumentValue"));
        Query query=new Query();
        query.fields().include("invitations");
        query.addCriteria(Criteria.where("_id").is(snackId));
        return template.findOne(query,Snack.class)
                .flatMapIterable(Snack::getInvitations);
    }


    /**
     * This method is used to remove a subscriptions of a given snack from the database
     * @param snackId the id of the snack from which to remove subscriptions
     * @param userId the id of user that have sent the subscription
     * @return a void  as a Mono
     */
    public Mono<Void> deleteSubscription(String snackId,String userId){
        Assert.notNull(snackId,env.getProperty("NullArgumentValue"));
        Assert.notNull(userId,env.getProperty("NullArgumentValue"));
        Update update=new Update();
        Query query=new Query();
        update.pull(SUBSCRIPTION_FIELD,new Query(Criteria.where("userId").is(userId)));
        query.addCriteria(Criteria.where("_id").is(snackId));
        FindAndModifyOptions options=new FindAndModifyOptions();
        options.returnNew(false);
        return template.findAndModify(query,update,options,Snack.class).then()
                ;
                 }
    /**
     * This method is used to remove a list of subscriptions of a given snack from the database
     * @param snackId the id of the snack from which to remove subscriptions
     * @param userIds a set of user id
     * @return a void  as a Mono
     */
    public Mono<Void> deleteSubscriptions(String snackId, Set<String> userIds){
        Assert.notNull(snackId,env.getProperty("NullArgumentValue"));
        Assert.notNull(userIds,env.getProperty("NullArgumentValue"));
        Update update=new Update();
        Query query=new Query();
        update.pull(SUBSCRIPTION_FIELD,new Query().addCriteria(Criteria.where("userId").in(userIds)));
        query.addCriteria(Criteria.where("_id").is(snackId));
        FindAndModifyOptions options=new FindAndModifyOptions();
        options.returnNew(false);
        return template.findAndModify(query,update,options,Snack.class).then()
                ;
                 }



    /**
     * This method is used to handle a subscription in order to accept or refuse a subscription
      * @param id the snack id
     * @param userId the subscribed user id
     * @param accept
     * @return a void  as a Mono
     */
    public Mono<Void> subscriptionHandler(String id,String userId,boolean accept){
        if (accept){
           return isSubscriber(id.toString(),userId).handle((aBoolean, synchronousSink) -> {
               if (!aBoolean){
                   synchronousSink.error(new ResourceNotFoundException(env.getProperty("SubscriptionService.handle.noSubscriber")));
               }
           }).then(customerService.addNewCustomer(id.toString(),new Customer(userId,null))
                   .then(deleteSubscription(id,userId))
                   .doOnSuccess(unused -> {
                       newSubscriptionSink.emitValue(new SubscriptionDetails(userId,id.toString()));
                   }))
            ;
        }
        return deleteSubscription(id,userId);
    }



    /**
     * This method is used to verify if a user is subscribed or is a customer
     * @param snackId the snack id through which make a verification
     * @param userId
     * @return a boolean representing the result
     */
    public Mono<Boolean> isSubscriberOrCustomer(String snackId,String userId){
        Query query=new Query();
        query.fields().include(SUBSCRIPTION_FIELD).include(CUSTOMERS_FIELD);
        query.addCriteria(Criteria.where("_id").is(snackId).and("subscriptions.userId").is(userId));
        return
                isSubscriber(snackId,userId).zipWith(isCustomer(snackId,userId),(isSubscriber,isCustomer)
                        -> isCustomer||isSubscriber);
    }
    public Mono<Boolean> isSubscriber(String snackId,String userId) {
        Query query = new Query();
        query.fields().include(SUBSCRIPTION_FIELD);
        query.addCriteria(Criteria.where("_id").is(snackId).and("subscriptions.userId").is(userId));
        return template.exists(query,Snack.class);
    }
    public Mono<Boolean> isCustomer(String snackId,String userId) {
        Query query = new Query();
        query.fields().include(CUSTOMERS_FIELD);
        query.addCriteria(Criteria.where("_id").is(snackId).and("customers.userId").is(userId));
        return template.exists(query,Snack.class);
    }
    public Mono<Boolean> isInvited(String snackId,String userId){
        Query query = new Query();
        query.fields().include("invitations");
        query.addCriteria(Criteria.where("_id").is(snackId).and("invitations").is(userId));
        return template.exists(query,Snack.class);
    }
}
