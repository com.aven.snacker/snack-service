package com.aven.snacker.snackservice.Services;

import com.aven.snacker.snackservice.cloud.notification.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Sinks;

@Service
public class NotificationService {
    private final Sinks.One<Notification> notificationSink;
    @Autowired
    public NotificationService(@Qualifier("notification-sink") Sinks.One<Notification> notificationSink) {
        this.notificationSink = notificationSink;
    }
    public void sendNotification(Notification notif){
        notificationSink.emitValue(notif);
    }
}
