package com.aven.snacker.snackservice.configuration.exceptionHandlerConfig;

import com.aven.snacker.snackservice.core.exceptions.*;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ServerWebInputException;

import java.util.Map;

@Component
public class CustomErrorAttributes extends DefaultErrorAttributes {
    private final Environment env;

    public CustomErrorAttributes(Environment env) {
        this.env = env;
    }

    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
        Map<String,Object> attributes= super.getErrorAttributes(request, options);
        Throwable error=getError(request);
        if (error instanceof ResourceNotFoundException){
            attributes.put("message",error.getMessage());
            attributes.put("status",404);
            attributes.put("error","Not found");
        }
       else if (error instanceof DuplicateEntityException){
            attributes.put("message",error.getMessage());
            attributes.put("status",409);
            attributes.put("error","Conflict");
        }
       else if (error instanceof NoSnackAttributedException){
            attributes.put("message",error.getMessage());
            attributes.put("status",403);
            attributes.put("error","403 Forbidden");
        }
       else if(error instanceof DeletedSnackException){
            attributes.put("message",error.getMessage());
            attributes.put("status",410);
            attributes.put("error","Resource deleted");
            return attributes;
        }
       else if(error instanceof DisabledSnackException){
            attributes.put("message",error.getMessage());
            attributes.put("status",403);
            attributes.put("error","Resource unavailable");
            return attributes;
        }
        else if (error instanceof EventSendingException){
            attributes.put("message",error.getMessage());
            attributes.put("status",500);
            attributes.put("error","Internal Server Error");
        }
        else if (error instanceof RestrictedOperationException){
            attributes.put("message",error.getMessage());
            attributes.put("status",403);
            attributes.put("error","Refused");
        }
        else if (error instanceof DomainException){
            attributes.put("message",error.getMessage());
            attributes.put("status",400);
            attributes.put("error","Bad request");
            return attributes;
        }else if (error instanceof WebExchangeBindException){
            attributes.put("message",(env.getProperty("validation.default")));
            return attributes;
        }else if (error instanceof ServerWebInputException){
            attributes.put("message",(env.getProperty("validation.default")));
            return attributes;
        }
        return attributes;
    }
}
