package com.aven.snacker.snackservice.configuration.exceptionHandlerConfig;

import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

import java.util.Map;

@Component
@Order(-2)
public class GlobalExceptionHandler extends AbstractErrorWebExceptionHandler {
    public GlobalExceptionHandler(CustomErrorAttributes attributes,
                                  ServerCodecConfigurer codecConfigurer,
                                  ApplicationContext context
                                  ){
        super(attributes,new ResourceProperties(),context);
        super.setMessageReaders(codecConfigurer.getReaders());
        super.setMessageWriters(codecConfigurer.getWriters());
    }
    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(final ErrorAttributes errorAttributes) {
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
    }
    private Mono<ServerResponse> renderErrorResponse(final ServerRequest request){
        getError(request).printStackTrace();
        Map<String, Object> attributes=getErrorAttributes(request,ErrorAttributeOptions.of(ErrorAttributeOptions.Include.MESSAGE));
        switch (getError(request).getClass().getName()){
            case "com.aven.snacker.snackservice.core.exceptions.InvalidCountryCodeException"
                    :return ServerResponse.badRequest().body(BodyInserters.fromValue(attributes));
            case "java.lang.NullPointerException":
            case "com.aven.snacker.snackservice.core.exceptions.EventSendingException"
                    :return ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).body(BodyInserters.fromValue(attributes));
            case "com.aven.snacker.snackservice.core.exceptions.DuplicateEntityException"
                    :return ServerResponse.status(HttpStatus.CONFLICT).body(BodyInserters.fromValue(attributes));
            case "com.aven.snacker.snackservice.core.exceptions.InvalidRequestParameterException":
            case "org.springframework.web.bind.support.WebExchangeBindException":
            case "org.springframework.web.server.ServerWebInputException"
                    : return ServerResponse.status(HttpStatus.BAD_REQUEST).body(BodyInserters.fromValue(attributes));
            case "org.springframework.web.server.MethodNotAllowedException"
                : return ServerResponse.status(HttpStatus.METHOD_NOT_ALLOWED).body(BodyInserters.fromValue(attributes));
            case "org.springframework.web.server.ResponseStatusException":
            case  "com.aven.snacker.snackservice.core.exceptions.ResourceNotFoundException"
                :  return ServerResponse.status(HttpStatus.NOT_FOUND).body(BodyInserters.fromValue(attributes));
            case "com.aven.snacker.snackservice.core.exceptions.NoSnackAttributedException":
            case "com.aven.snacker.snackservice.core.exceptions.DisabledSnackException":
            case "com.aven.snacker.snackservice.core.exceptions.RestrictedOperationException"
                : return ServerResponse.status(HttpStatus.FORBIDDEN).body(BodyInserters.fromValue(attributes));
            case "com.aven.snacker.snackservice.core.exceptions.DeletedSnackException"
                : return ServerResponse.status(HttpStatus.GONE).body(BodyInserters.fromValue(attributes));
            default:return ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
