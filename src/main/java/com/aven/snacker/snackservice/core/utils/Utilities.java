package com.aven.snacker.snackservice.core.utils;

import java.util.*;

public class Utilities {
    /**
     * this class is a helper method used to initialize  a map if it's null and add a new element to it
     * @param key
     * @param value
     * @param map
     */

    public static void addInMap(Object key,Object value, Map map){
        if (map==null){
            map=new HashMap();
        }
        map.put(key,value);
    }
    /**
     * this class is a helper method used to initialize  a Set if it's null and add a new element to it
     * @param value
     * @param set
     */
    public static void addInSet(Object value, Set set){
        if(set==null){
            set=new HashSet();
        }
        set.add(value);
    }
    /**
     * this class is a helper method used to initialize  a map if it's null and add a new element to it
     * @param value
     * @param list
     */
    public static void addInList(Object value, List list){
        if(list==null){
            list=new ArrayList();
        }
        list.add(value);
    }
}
