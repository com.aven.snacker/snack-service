package com.aven.snacker.snackservice.core.exceptions;

public class InvalidCountryCodeException extends DomainException{
    public InvalidCountryCodeException(String message){
        super(message);
    }
}
