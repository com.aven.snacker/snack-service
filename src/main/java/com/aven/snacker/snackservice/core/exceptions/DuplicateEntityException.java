package com.aven.snacker.snackservice.core.exceptions;

public class DuplicateEntityException extends DomainException{

    public DuplicateEntityException(String message) {
        super(message);
    }
}
