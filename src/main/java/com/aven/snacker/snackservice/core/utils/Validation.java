package com.aven.snacker.snackservice.core.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {
    final static String EMAIL="^(.+)@(.+)$";
    public static boolean email(String mail){
        Pattern pattern=Pattern.compile(EMAIL);
        Matcher matcher= pattern.matcher(mail);
        return matcher.matches();
    }
}
