package com.aven.snacker.snackservice.core.users;

public interface SimpleUserContract {
    String getUserId();
}
