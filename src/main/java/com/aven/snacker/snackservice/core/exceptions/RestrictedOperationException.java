package com.aven.snacker.snackservice.core.exceptions;

public class RestrictedOperationException extends DomainException{
    public RestrictedOperationException(String message) {
        super(message);
    }
}
