package com.aven.snacker.snackservice.core.users;


import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

public class BasicUserDetails implements UserDetailsContract, Serializable {
    @Indexed
    @NotNull
    @Size(max = 100)
    @NotEmpty
    protected String username;
    @NotNull
    @Size(max = 100)
    @NotEmpty
    protected String firstName;
    @NotNull
    @Size(max = 100)
    @NotEmpty
    protected String lastName;
    @Email
    @NotNull
    @Size(max = 100)
    @NotEmpty
    protected String email;
    @Indexed
    @NotNull
    protected long phoneNumber;
    public boolean isSnack;
    protected String snackId;
    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSnackId() {
        return snackId;
    }

    public void setSnackId(String snackId) {
        this.snackId = snackId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicUserDetails basicUser = (BasicUserDetails) o;
        return getUsername().equals(basicUser.getUsername());
    }


    @Override
    public int hashCode() {
        return Objects.hash(getUsername());
    }

    @Override
    public String getUserId() {
        return username;
    }
}
