package com.aven.snacker.snackservice.core.exceptions;

public class NoSnackAttributedException extends DomainException{
    public NoSnackAttributedException(String message) {
        super(message);
    }
}
