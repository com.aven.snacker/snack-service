package com.aven.snacker.snackservice.core.geolocation;

public interface Localisable {
    String getLocationId();
}
