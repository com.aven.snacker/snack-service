package com.aven.snacker.snackservice.core.users;

import com.aven.snacker.snackservice.core.utils.Utilities;
import com.aven.snacker.snackservice.core.users.invitations.DefaultInvitation;
import com.aven.snacker.snackservice.core.snack.Snack;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;

@Document
@TypeAlias("snack_user")
/**
 * this class is a representation of a user of snacker.
 * It defines all the informations about a snacker user
 */
public class SnackerSimpleUserContract implements SimpleUserContract {
    @Id
    ObjectId id;
    @Transient
    @Autowired
    private Environment env;
    private String userId;
    private Map<Snack,Long> subscribedSnacks;
    private Map<Snack, Long> closestSnacks;
    private String locationId;
    private List<DefaultInvitation> invitations;

    /**
     * Used for adding a new Snack to the subscribed snack collection
     * @param snack
     * @param distance
     */
    public void addNewSubscription(Snack snack,long distance){
        Assert.notNull(snack,env.getProperty("SnackUser.addNewSubscription.nullSnack"));
        Utilities.addInMap(snack,distance,subscribedSnacks);
    }
    /**
     * Used for adding a new Snack to the closest snack collection
     * @param snack
     * @param distance
     */
    public void addCloseSnack(Snack snack,long distance){
        Assert.notNull(snack,env.getProperty("SnackUser.addCloseSnack.nullSnack"));
        Utilities.addInMap(snack,distance,closestSnacks);
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Map<Snack, Long> getSubscribedSnacks() {
        return subscribedSnacks;
    }

    public void setSubscribedSnacks(Map<Snack, Long> subscribedSnacks) {
        this.subscribedSnacks = subscribedSnacks;
    }

    public Map<Snack, Long> getClosestSnacks() {
        return closestSnacks;
    }

    public void setClosestSnacks(Map<Snack, Long> closestSnacks) {
        this.closestSnacks = closestSnacks;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public List<DefaultInvitation> getInvitations() {
        return invitations;
    }

    public void setInvitations(List<DefaultInvitation> invitations) {
        this.invitations = invitations;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
