package com.aven.snacker.snackservice.core.users;

import com.aven.snacker.snackservice.core.evaluations.Rate;

/**
 * This class represent a user subscribed to a snack and provide all information and processes that involve a user and a particular snack
 */
public class Customer implements SimpleUserContract {
    public Customer() {
    }

    public Customer(String userId, Rate rate) {
        this.userId = userId;
        this.rate = rate;
    }

    private  String userId;
    private Rate rate;

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }


    @Override
    public String getUserId() {
        return userId;
    }

}
