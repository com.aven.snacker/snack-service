package com.aven.snacker.snackservice.core.users.subscriptions;

public class SubscriptionDetails {
    private final String userId;
    private final String snackId;

    public SubscriptionDetails(String userId, String snackId) {
        this.userId = userId;
        this.snackId = snackId;
    }

    public String getSnackId() {
        return snackId;
    }

    public String getUserId() {
        return userId;
    }
}
