package com.aven.snacker.snackservice.core.exceptions;

public class ResourceNotFoundException extends DomainException{
    public ResourceNotFoundException(String message) {
        super(message);
    }
}
