package com.aven.snacker.snackservice.core.exceptions;

public class InvalidRequestParameterException extends DomainException{
    public InvalidRequestParameterException(String message) {
        super(message);
    }
}
