package com.aven.snacker.snackservice.core.snack;

import java.time.LocalDateTime;
import java.util.Date;

public class AccessCode {
    private String code;
    private LocalDateTime createdAt;

    public AccessCode() {
    }

    public AccessCode(String code, LocalDateTime createdAt) {
        this.code = code;
        this.createdAt = createdAt;
    }
}
