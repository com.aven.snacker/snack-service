package com.aven.snacker.snackservice.core.evaluations;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * this class represent an evaluation of a snack or eventually a customer
 */
public class Rate {
    @Size(max = 5)
    private int rate;
    @Size(max = 255)
    private String comment;
    private LocalDateTime date;


    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
