package com.aven.snacker.snackservice.core.users.subscriptions;

import com.aven.snacker.snackservice.cloud.notification.Notifiable;

import java.time.LocalDateTime;

/**
 * this class represent a subscription to a snack by a user
 */
public class Subscription implements Notifiable {
    public Subscription(LocalDateTime date, String userId) {
        this.date = date;
        this.userId = userId;
    }
    private final LocalDateTime date;
    private final String userId;


    @Override
    public LocalDateTime getDate() {
        return date;
    }

    public String getUserId() {
        return userId;
    }
}
