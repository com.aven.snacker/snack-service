package com.aven.snacker.snackservice.core.exceptions;

import com.aven.snacker.snackservice.cloud.messaging.EventDetails;

public class EventSendingException extends DomainException{
    public EventSendingException(String message,EventDetails details) {
        super(message);
        eventDetails=details;
    }
    private final EventDetails eventDetails;

    public EventDetails getEventDetails() {
        return eventDetails;
    }
}
