package com.aven.snacker.snackservice.core.users.invitations;

import java.util.Date;

/**
 * This class represent an invitation to subscribe to a snack
 */
public class DefaultInvitation implements Invitation{
    public DefaultInvitation(String userId, String snackId, Date date) {
        this.userId = userId;
        this.snackId = snackId;
        this.date = date;
    }


    private final String userId;
    private final String snackId;
    private final Date date;

    public String getUserId() {
        return userId;
    }

    public String getSnackId() {
        return snackId;
    }

    public Date getDate() {
        return date;
    }
}
