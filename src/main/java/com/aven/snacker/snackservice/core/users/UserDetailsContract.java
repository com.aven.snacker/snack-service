package com.aven.snacker.snackservice.core.users;

/**
 * This interface is a contract that define a valid user of the platform "AVEN"
 */
public interface UserDetailsContract extends SimpleUserContract {
    String getUsername();
    String getFirstName();
    String getLastName();
    Long getPhoneNumber();
    String getEmail();
}
