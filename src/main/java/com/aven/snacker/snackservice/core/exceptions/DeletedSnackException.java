package com.aven.snacker.snackservice.core.exceptions;

public class DeletedSnackException extends DomainException{
    public DeletedSnackException(String message) {
        super(message);
    }
}
