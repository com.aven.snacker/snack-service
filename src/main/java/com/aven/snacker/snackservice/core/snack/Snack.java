package com.aven.snacker.snackservice.core.snack;

import com.aven.snacker.snackservice.core.geolocation.Address;
import com.aven.snacker.snackservice.core.utils.Utilities;
import com.aven.snacker.snackservice.core.users.Customer;
import com.aven.snacker.snackservice.core.users.subscriptions.Subscription;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.data.annotation.*;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.Assert;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.*;

@Document
@TypeAlias("snack")
/**
 * This class is a representation of a snack it define all information related to a snack
 */
public class Snack implements Persistable<String> {
    public Snack() {
    }
    @Id
    private String id;
    @Transient
    @JsonIgnore
    @Autowired
    Environment env;
    @Size(max = 100)
    @NotNull
    private String name;
    private float quote;
    private long customerCount;
    @Size(max = 500)
    private String logoUri;
    @CreatedDate
    @Order(Ordered.LOWEST_PRECEDENCE-2)
    private Date creationDate;
    private LocalDateTime deletionDate;
    private boolean enabled;
    private boolean deleted;
    private long phoneNumber;
    @Size(max = 500)
    private String email;
    @Size(max = 10)
    @NotNull
    @NotEmpty
    private String countryCode;
    @Size(max = 10)
    @NotNull
    @NotEmpty
    private String regionCode;
    @Indexed
    private String owner;
    //Subscribed customers list
    private Set<Customer> customers;
    private Set<String> invitations;
    @NotNull
    private Address address;
    private List<Subscription> subscriptions;
    private final AccessCode accessCode=null;
    @LastModifiedDate
    @Order(Ordered.LOWEST_PRECEDENCE)
    private Date lastUpdate;
    private Boolean isNew=true;
    /**
     * Used for adding a new customer to the collection "customers"
     * @param customer
     */
    public void addNewCustomer(Customer customer){
        Assert.notNull(customer,env.getProperty("Snack.addNewCustomer.nullCustomer"));
        Utilities.addInSet(customer,customers);
    }

    /**
     * Used for adding a new subscription to the collection "subscription"
     * @param subscription
     */
    public void addNewSubscription(Subscription subscription){
        Assert.notNull(subscription,env.getProperty("Snack.addNewCustomer.nullSubscription"));
        Utilities.addInList(subscription,subscriptions);
    }

    public void initialize(){
        id=null;
        quote=0;
        customerCount=0;
        deletionDate=null;
        enabled=true;
        deleted=false;
        customers=new HashSet<>();
        subscriptions=new ArrayList<>();
        invitations=new HashSet<>();
    }


    //--------getters and setters--------------


    public String getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getQuote() {
        return quote;
    }

    public void setQuote(float quote) {
        this.quote = quote;
    }

    public long getCustomerCount() {
        return customerCount;
    }

    public void setCustomerCount(long customerCount) {
        this.customerCount = customerCount;
    }

    public String getLogoUri() {
        return logoUri;
    }

    public void setLogoUri(String logoUri) {
        this.logoUri = logoUri;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getDeletionDate() {
        return deletionDate;
    }

    public void setDeletionDate(LocalDateTime deletionDate) {
        this.deletionDate = deletionDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Set<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(Set<Customer> customers) {
        this.customers = customers;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }


    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Environment getEnv() {
        return env;
    }

    public void setEnv(Environment env) {
        this.env = env;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<String> getInvitations() {
        return invitations;
    }

    public void setInvitations(Set<String> invitations) {
        this.invitations = invitations;
    }

    public AccessCode getAccessCode() {
        return accessCode;
    }
    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }



    public Boolean getNew() {
        return isNew;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Snack)) return false;
        Snack snack = (Snack) o;
        return getId().equals(snack.getId()) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Snack{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", quote=" + quote +
                ", customerCount=" + customerCount +
                ", logoUri='" + logoUri + '\'' +
                ", creationDate=" + creationDate +
                ", deletionDate=" + deletionDate +
                ", enabled=" + enabled +
                ", deleted=" + deleted +
                ", phoneNumber=" + phoneNumber +
                ", email='" + email + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", RegionCode='" + regionCode + '\'' +
                ", owner='" + owner + '\'' +
                ", customers=" + customers +
                ", address=" + address +
                ", subscriptions=" + subscriptions +
                '}';
    }
}
