package com.aven.snacker.snackservice.core.users.invitations;

import java.util.Date;

public interface Invitation {
    Date getDate();
    String getUserId();
    String getSnackId();
}
