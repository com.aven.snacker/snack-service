package com.aven.snacker.snackservice.core.utils;

import java.util.Collection;

public class ElementsPage<T> {
    private  long total;
    private  long offset;
    private Collection<T> content;

    public ElementsPage() {
    }

    public ElementsPage(int total, int offset, Collection<T> content) {
        this.total = total;
        this.offset = offset;
        this.content = content;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public Collection<T> getContent() {
        return content;
    }

    public void setContent(Collection<T> content) {
        this.content = content;
    }
}
