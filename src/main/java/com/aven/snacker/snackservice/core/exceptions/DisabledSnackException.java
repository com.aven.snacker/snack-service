package com.aven.snacker.snackservice.core.exceptions;

public class DisabledSnackException extends DomainException{

    public DisabledSnackException(String message) {
        super(message);
    }
}
