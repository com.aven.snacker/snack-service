package com.aven.snacker.snackservice.core.geolocation;

public class Address implements Localisable{
    private String address;
    private String locationId;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
}
