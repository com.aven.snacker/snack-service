package com.aven.snacker.snackservice.cloud.notification;

public enum NotificationTag {
    SUBSCRIPTION("subscription");
    String tag;
    NotificationTag(String tag) {
        this.tag=tag;
    }

    public String getTag() {
        return tag;
    }
}
