package com.aven.snacker.snackservice.cloud.messaging;

import java.util.Date;
public class EventDetails {
    private final String topic;
    private final Object payload;
    private final Date emissionDate;


    public EventDetails(String topic, Object payload, Date emissionDate) {
        this.topic = topic;
        this.payload = payload;
        this.emissionDate = emissionDate;
    }

    public String getTopic() {
        return topic;
    }

    public Object getPayload() {
        return payload;
    }

    public Date getEmissionDate() {
        return emissionDate;
    }
}
