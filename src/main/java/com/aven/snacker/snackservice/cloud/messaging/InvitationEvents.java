package com.aven.snacker.snackservice.cloud.messaging;

import com.aven.snacker.snackservice.core.users.invitations.DefaultInvitation;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoProcessor;
import reactor.core.publisher.Sinks;

import java.util.function.Supplier;

@Configuration
public class InvitationEvents {
    @Bean
    public Supplier<Mono<DefaultInvitation>> invitation(){
        return ()-> MonoProcessor.fromSink(getInvitationSink());
    }

    @Bean
    @Qualifier("invitation-sink")
    Sinks.One<DefaultInvitation> getInvitationSink(){
        return Sinks.one();
    }
}
