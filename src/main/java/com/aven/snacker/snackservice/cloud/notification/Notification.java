package com.aven.snacker.snackservice.cloud.notification;

public interface Notification {
    String getNotificationTag();
    Object getPayload();
}
