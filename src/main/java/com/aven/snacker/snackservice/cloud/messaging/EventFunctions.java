package com.aven.snacker.snackservice.cloud.messaging;

import com.aven.snacker.snackservice.core.snack.Snack;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.*;

import java.util.Map;
import java.util.function.Supplier;

@Configuration
public class EventFunctions {

    @Bean
    public Supplier<Mono<Snack>> newSnack() {
        return ()-> MonoProcessor.fromSink(getCreateSnackSink());
    }
    @Bean
    public Supplier<Mono<Map<String,String>>> snackDeleted(){
        return ()->MonoProcessor.fromSink(getSnackDeletedSink());
    }
    @Bean
    public Supplier<Mono<String>> snackBackup(){
        return ()->MonoProcessor.fromSink(getSnackBackupSink());
    }

    @Bean
    @Qualifier("new-snack-processor")
    public Sinks.One<Snack> getCreateSnackSink(){
        return Sinks.one();
    }
    @Bean
    @Qualifier("snack-deleted-processor")
    public Sinks.One<Map<String,String>> getSnackDeletedSink(){
        return Sinks.one();
    }
    @Bean
    @Qualifier("snack-backup-processor")
    public Sinks.One<String> getSnackBackupSink(){
        return Sinks.one();
    }
}
