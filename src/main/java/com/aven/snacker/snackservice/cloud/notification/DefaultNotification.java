package com.aven.snacker.snackservice.cloud.notification;

public class DefaultNotification<T> implements Notification {
    private final T payload;
    private final String tag;

    public DefaultNotification(T payload, NotificationTag tag) {
        this.payload = payload;
        this.tag = tag.getTag();
    }

    @Override
    public String getNotificationTag() {
        return tag;
    }

    @Override
    public Object getPayload() {
        return payload;
    }

}
