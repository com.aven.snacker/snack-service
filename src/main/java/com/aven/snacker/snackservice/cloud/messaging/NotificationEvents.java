package com.aven.snacker.snackservice.cloud.messaging;

import com.aven.snacker.snackservice.cloud.notification.Notification;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoProcessor;
import reactor.core.publisher.Sinks;

import java.util.function.Supplier;

@Configuration
public class NotificationEvents {
    @Bean
    public Supplier<Mono<Notification>> sendNotification(){
        return ()->MonoProcessor.fromSink(this.getNotificationSink());
    }
    @Bean
    @Qualifier("notification-sink")
    public Sinks.One<Notification> getNotificationSink(){
        return Sinks.one();
    }
}
