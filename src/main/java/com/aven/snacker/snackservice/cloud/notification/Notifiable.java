package com.aven.snacker.snackservice.cloud.notification;

import java.time.LocalDateTime;

public interface Notifiable {
    public LocalDateTime getDate();
}
