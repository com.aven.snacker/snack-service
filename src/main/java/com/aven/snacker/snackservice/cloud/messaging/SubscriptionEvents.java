package com.aven.snacker.snackservice.cloud.messaging;

import com.aven.snacker.snackservice.core.users.Customer;
import com.aven.snacker.snackservice.core.users.subscriptions.SubscriptionDetails;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoProcessor;
import reactor.core.publisher.Sinks;

import java.util.function.Supplier;

@Configuration
public class SubscriptionEvents {
    @Bean
    public Supplier<Mono<SubscriptionDetails>> newSubscriber(){
        return ()-> MonoProcessor.fromSink(getNewSubscriberSink());
    }
    @Bean
    @Qualifier("new-subscriber-sink")
    public Sinks.One<SubscriptionDetails> getNewSubscriberSink(){
        return Sinks.one();
    }
    @Bean
    public Supplier<Mono<Customer>> newCustomer(){
       return ()->MonoProcessor.fromSink(getNewCustomerSink());
    }
    @Bean
    @Qualifier("new-customer-sink")
    public Sinks.One<Customer> getNewCustomerSink(){
        return Sinks.one();
    }
}
