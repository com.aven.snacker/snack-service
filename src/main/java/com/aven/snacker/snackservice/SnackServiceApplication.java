package com.aven.snacker.snackservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.config.EnableReactiveMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@PropertySources({@PropertySource("classpath:messages.properties"),@PropertySource("classpath:domain.properties")})
@EnableReactiveMongoAuditing
@EnableReactiveMongoRepositories
public class SnackServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SnackServiceApplication.class, args);
	}

}
