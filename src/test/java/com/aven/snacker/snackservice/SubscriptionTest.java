package com.aven.snacker.snackservice;

import com.aven.snacker.snackservice.core.geolocation.Address;
import com.aven.snacker.snackservice.core.snack.Snack;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.Objects;

import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockJwt;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SnackServiceApplication.class)
@AutoConfigureWebTestClient
public class SubscriptionTest {
    @Autowired
    WebTestClient webTestClient;
    Snack snack=new Snack();
    static String id;
    Jwt jwt ;
    @Before
    public void prepare(){
        webTestClient.mutateWith(csrf()).delete().uri("/snacker/snack-service/snacks/remove?owner=test")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk();
        snack.setName("test");
        snack.setOwner("test");
        Address address=new Address();
        address.setAddress("test");
        address.setLocationId("dfsq");
        snack.setAddress(address);
        snack.setCountryCode("SN");
        snack.setRegionCode("dk");
        jwt=  Jwt.withTokenValue("token")
                .header("alg", "none")
                .claim("sub", "test")
                .claim("scope", "read")
                .claim("preferred_username","test@test.com")
                .claim("given-name","Mustapha")
                .claim("family_name","Fall")
                .claim("phone_number","064653365")
                .claim("email","mouhamadoumoustapha.fall@e-polytechnique.ma")
                .claim("scope","READ_ALL_SNACKS")
                .build()
        ;
        webTestClient.mutateWith(mockJwt().jwt(jwt)).mutateWith(csrf()).post().uri("/snacker/snack-service/snacks")
                .bodyValue(snack)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isCreated()
                .expectBody(Snack.class)
                .consumeWith(result->{
                    id=result.getResponseBody().getId().toString();
                    Assert.assertEquals(result.getResponseBody().getName(),"test");
                });
    }
    @Test
    @WithMockUser(username = "test")
    public void testSubscription(){
        webTestClient.mutateWith(mockJwt().jwt(jwt)).mutateWith(csrf()).post().uri("/snacker/snack-service/snacks/"+id+"/subscribe")
                .exchange()
                .expectStatus().isOk();
       Flux<Snack> snackMono= webTestClient.mutateWith(csrf()).get().uri("/snacker/snack-service/snacks?id="+id)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .returnResult(Snack.class)
                .getResponseBody()

        ;
        StepVerifier.create(snackMono)
                .expectNextCount(1)
                .assertNext(snack1 -> Objects.equals(snack1.getSubscriptions(),1));
    }
}
