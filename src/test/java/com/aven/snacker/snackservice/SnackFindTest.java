package com.aven.snacker.snackservice;

import com.aven.snacker.snackservice.core.geolocation.Address;
import com.aven.snacker.snackservice.core.snack.Snack;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SnackServiceApplication.class)
@AutoConfigureWebTestClient
public class SnackFindTest {
    @Autowired
    WebTestClient webTestClient;
    Snack snack=new Snack();
    Jwt jwt ;
    @Before
    public void prepare(){
        snack.setName("test");
        snack.setOwner("test");
        Address address=new Address();
        address.setAddress("test");
        address.setLocationId("dfsq");
        snack.setAddress(address);
        snack.setCountryCode("SN");
        snack.setRegionCode("dk");
        jwt=  Jwt.withTokenValue("token")
                .header("alg", "none")
                .claim("sub", "test")
                .claim("scope", "read")
                .claim("preferred_username","test@test.com")
                .claim("given-name","Mustapha")
                .claim("family_name","Fall")
                .claim("phone_number","064653365")
                .claim("email","mouhamadoumoustapha.fall@e-polytechnique.ma")
                .claim("scope","READ_ALL_SNACKS")
                .build()
        ;
        webTestClient.mutateWith(mockJwt().jwt(jwt)).mutateWith(csrf()).post().uri("/snacker/snack-service/snacks")
                .bodyValue(snack)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isCreated()
                .expectBody(Snack.class)
                .consumeWith(result->{
                    snack.setId(result.getResponseBody().getId());
                    Assert.assertEquals(result.getResponseBody().getName(),"test");
                });
    }
    @Test
    @WithMockUser(username = "test",authorities = {"READ_ALL_SNACKS"})
    public void findAllTest(){
        webTestClient
                .mutateWith(csrf()).get().uri("/snacker/snack-service/snacks")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(Snack.class)
                .consumeWith(result->{
                    Assert.assertEquals(result.getResponseBody().stream().anyMatch(snack1 -> snack1.getName().equals("test")),true);
                })
                ;
    }
    @Test
    @WithMockUser(username = "test")
    public void findOneWithOwner(){
        Flux<Snack> snackFlux=webTestClient.mutateWith(csrf()).get().uri("/snacker/snack-service/snacks?owner=test")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .returnResult(Snack.class)
                .getResponseBody()
                ;
        StepVerifier.create(snackFlux)
                .expectNextCount(0);
    }
    @Test
    @WithMockUser(username = "test")
    public void findOneWithPhoneNumber(){
        Flux<Snack> snackFlux=webTestClient.mutateWith(csrf()).get().uri("/snacker/snack-service/snacks?phone=064653365")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isForbidden()
                .returnResult(Snack.class)
                .getResponseBody()
                ;
        StepVerifier.create(snackFlux)
                .expectNextCount(1);
    }
    @Test
    @WithMockUser(username = "test")
    public void findByName(){
        Flux<Snack> snackFlux=webTestClient.mutateWith(csrf()).get().uri("/snacker/snack-service/snacks?name=test")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .returnResult(Snack.class)
                .getResponseBody()
                ;
        StepVerifier.create(snackFlux)
                .expectNextCount(1);
    }

    @After
    public void clean(){
        webTestClient.mutateWith(csrf()).delete().uri("/snacker/snack-service/snacks/remove?owner=test")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk();
    }
}
