package com.aven.snacker.snackservice;

import com.aven.snacker.snackservice.core.geolocation.Address;
import com.aven.snacker.snackservice.core.snack.Snack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockJwt;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SnackServiceApplication.class)
@AutoConfigureWebTestClient
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class SnackCreationTest {
    @Autowired
    WebTestClient webTestClient;
    Snack snack=new Snack();
    Jwt jwt ;
    @Before
    public void prepare(){
        snack.setName("test");
        Address address=new Address();
        address.setAddress("test");
        address.setLocationId("dfsq");
        snack.setAddress(address);
        snack.setCountryCode("SN");
        snack.setRegionCode("dk");
        jwt=  Jwt.withTokenValue("token")
                .header("alg", "none")
                .claim("sub", "user")
                .claim("scope", "read")
                .claim("preferred_username","mouhamadoumoustapha.fall@e-polytechnique.ma")
                .claim("given-name","Mustapha")
                .claim("family_name","Fall")
                .claim("phone_number","064653365")
                .claim("email","mouhamadoumoustapha.fall@e-polytechnique.ma")
                .build()
                ;
    }
    @Test
    @Order(1)
    @WithMockUser(username = "test",authorities = "admin")
    public void newSnackTest(){
        webTestClient.mutateWith(mockJwt().jwt(jwt)).mutateWith(csrf()).post().uri("/snacker/snack-service/snacks")
                .bodyValue(snack)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isCreated()
                .expectBody(Snack.class)
                .consumeWith(result->{
                    Assert.assertEquals(result.getResponseBody().getName(),"test");
                });
        webTestClient.mutateWith(mockJwt().jwt(jwt)).mutateWith(csrf()).post().uri("/snacker/snack-service/snacks")
                .bodyValue(snack)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isEqualTo(409);
    }
    @Test
    @WithMockUser(username = "test2")
    @Order(2)
    public void newSnackWithInvalidData(){
        snack.setName(null);
        webTestClient.mutateWith(mockJwt().jwt(jwt)).mutateWith(csrf()).post().uri("/snacker/snack-service/snacks")
                .bodyValue(snack)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest()
                ;
    }

    @AfterAll
    public void clean(){
        webTestClient.mutateWith(csrf()).delete().uri("/snacker/snack-service/snacks/remove?phone=064653365")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk();
    }
}
